from mimesis import Person
from pytest_testconfig import config
from selenium.common.exceptions import NoSuchElementException, TimeoutException
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as ec
from selenium.webdriver.support.wait import WebDriverWait
import re

from pages.contracts_page import ContractsPage
from pages.header import Header
from pages.invitation_page import InvitationPage
from pages.pension_page import PensionPage
from pages.products_page import ProductsPage


def test_contracts(driver):
    contracts = driver.find_elements_by_xpath(ContractsPage.CONTRACT_STATUS_CONTAINER)
    contract_statuses = []
    for item in contracts:
        if item.text == config['contract_statuses']['gut versichert']:
            contract_statuses.append(item.text)
    assert len(contract_statuses) >= 3, 'less than 3 GUT VERSICHERT contracts found'


def test_pension(driver):
    driver.find_element_by_xpath(Header.RENTE_TAB).click()
    try:
        WebDriverWait(driver, 5).\
            until(ec.text_to_be_present_in_element((By.XPATH, PensionPage.PENSION_INCOME_CONTAINER), '€'))
    except TimeoutException:
        WebDriverWait(driver, 5).\
            until(ec.visibility_of_element_located((By.XPATH, PensionPage.PENSION_INCOME_CONTAINER)))
    pension_income_value = driver.find_element_by_xpath(PensionPage.PENSION_INCOME_CONTAINER).text
    pension_income_value = int(re.sub(r'\D', '', pension_income_value)[:-2])
    assert pension_income_value > 2400, 'renteneinkommen_value less than €2400'


def test_products(driver):
    driver.find_element_by_xpath(Header.BEDARF_TAB).click()
    driver.find_element_by_xpath(ProductsPage.POPUP_CLOSE_BTN).click()
    label = driver.find_element_by_xpath(ProductsPage.ALL_PRODUCTS_BOUGHT_LBL_CONTAINER).text
    assert label == config['labels']['all products bought'], \
        'Du hast alle empfoxlenen produkte label not found'


def test_invitation(driver):
    driver.find_element_by_xpath(Header.BEDARF_TAB).click()
    driver.find_element_by_xpath(ProductsPage.POPUP_CLOSE_BTN).click()
    driver.find_element_by_xpath(ProductsPage.RECOMMEND_NOW_LNK).click()
    person = Person('en')
    driver.find_element_by_xpath(InvitationPage.EMAIL_FLD).send_keys(person.email())
    driver.find_element_by_xpath(InvitationPage.SEND_EMAIL_BTN).click()
    try:
        success_message = driver.find_element_by_xpath(InvitationPage.SUCCESS_MSG_CONTAINER).text
    except NoSuchElementException:
        WebDriverWait(driver, 5).\
            until(ec.visibility_of_element_located((By.XPATH, InvitationPage.SUCCESS_MSG_CONTAINER)))
        success_message = driver.find_element_by_xpath(InvitationPage.SUCCESS_MSG_CONTAINER).text
    assert success_message == config['labels']['success invitation'], 'success message not displayed'
