import pytest
from pytest_testconfig import config
from selenium import webdriver
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as ec
from selenium.webdriver.support.wait import WebDriverWait

from pages.main_page import MainPage


@pytest.fixture()
def driver(request):
    options = webdriver.ChromeOptions()
    options.add_argument('--no-sandbox')
    options.add_argument('headless')
    options.add_argument('disable-infobars')
    options.add_argument('window-size=1920x1080')
    options.add_argument('--verbose')
    options.add_argument('--disable-extensions')
    options.add_argument('--disable-gpu')
    options.add_argument('--disable-dev-shm-usage')
    wd = webdriver.Chrome(options=options)
    wd.implicitly_wait(5)
    request.addfinalizer(wd.quit)
    return wd


@pytest.fixture(autouse=True)
def login(driver):
    driver.get(config['urls']['base_url'])
    driver.find_element_by_xpath(MainPage.LOGIN_BTN).click()
    WebDriverWait(driver, 5).until(ec.invisibility_of_element_located((By.XPATH, MainPage.LOGIN_BTN)))
    ActionChains(driver).move_to_element(driver.find_element_by_xpath(MainPage.EMAIL_FLD)).perform()
    driver.find_element_by_xpath(MainPage.EMAIL_FLD).send_keys(config['credentials']['username'])
    driver.find_element_by_xpath(MainPage.PWD_FLD).send_keys(config['credentials']['password'])
    driver.find_element_by_xpath(MainPage.SUBMIT_BTN).click()
