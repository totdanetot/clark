class ProductsPage:
    POPUP_CLOSE_BTN = '//i[@class="ember-modal__body__close  cucumber-no1-modal-close"]'
    ALL_PRODUCTS_BOUGHT_LBL_CONTAINER = '//h1[@class="manager__optimisations__optimisation-list__done__title"]'
    RECOMMEND_NOW_LNK = '//a[@class="manager__optimisations__optimisation-list__done__link--text"]'
