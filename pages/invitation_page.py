class InvitationPage:
    EMAIL_FLD = '//input[@id="email"]'
    SEND_EMAIL_BTN = '//button[@id="sendInvitationEmail"]'
    SUCCESS_MSG_CONTAINER = '//div[@class="success-text"]'
